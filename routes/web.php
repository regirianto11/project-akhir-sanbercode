<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/book','BookController@index');
Route::get('/book/create','BookController@create');
Route::post('/book','BookController@store');
Route::put('/book/{id}','BookController@update');
Route::get('/book/{id}','BookController@show');
Route::get('/book/{id}/edit','BookController@edit');
Route::delete('/book/{id}','BookController@destroy');


Route::get('/user','UserController@index');
Route::get('/user/{id}','UserController@show');

Route::get('/categorie','CategorieController@index');
Route::get('/categorie/create','CategorieController@create');
Route::post('/categorie/create','CategorieController@store');
Route::get('/categorie/{id}/edit','CategorieController@edit');
Route::put('/categorie/{id}','CategorieController@update');
Route::delete('/categorie/{id}','CategorieController@destroy');