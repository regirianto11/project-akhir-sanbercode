<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users=User::select('id','name', 'email', 'telp','created_at')->get();
        return view('user.index',compact('users'));
    }

    public function show($id){
        $user=User::find($id);
        return view('user.show',compact('user'));

    }
}
