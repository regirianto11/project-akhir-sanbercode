<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Categorie;
use File;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        $books=Book::all();
        return view('book.index',compact('books'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Categorie::All();
        
        return view('book.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'author' => 'required',
            'year' => 'required',
            'categorie' => 'required',
            'cover' => 'mimes:jpeg,bmp,png|max:2200'
        ]);

        $cover = $request->cover;
        $new_cover = time() . " - " . $cover->getClientOriginalName();

        $book=Book::create([
            'title' => $request['title'],
            'author' => $request['author'],
            'categorie_id' => $request['categorie'],
            'year' => $request['year'],
            'cover' => $new_cover,
            'plot' => $request['plot'],
        ]);

        $cover->move('cover/', $new_cover);

        return redirect('/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book=Book::find($id);
        return view('book.show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Categorie::All();
        $book=Book::find($id);
        return view('book.edit',compact('book','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'author' => 'required',
            'year' => 'required',
            'categorie' => 'required',
            'cover' => 'mimes:jpeg,bmp,png|max:2200'
        ]);

        $book = Book::findorfail($id);

        if ($request->has('cover')) {
            # code...
            $path = "cover/";
            File::delete($path . $book->cover);
            $cover = $request->cover;
            $new_cover = time() . " - " . $cover->getClientOriginalName();
            $cover->move('cover/', $new_cover);
            $book_data = [
                'title' => $request['title'],
                'author' => $request['author'],
                'categorie_id' => $request['categorie'],
                'year' => $request['year'],
                'cover' => $new_cover,
                'plot' => $request['plot'],
            ];
        } else {
            $book_data = [
                'title' => $request['title'],
                'author' => $request['author'],
                'categorie_id' => $request['categorie'],
                'year' => $request['year'],
                'plot' => $request['plot'],
            ];
        }

        // $book=Book::where('id',$id)
        //   ->update([
        //     'title' => $request['title'],
        //     'author' => $request['author'],
        //     'categorie' => $request['categorie'],
        //     'year' => $request['year'],
        //     'cover' => $request['cover'],
        //     'plot' => $request['plot'],
        // ]);


        $book->update($book_data);
        return redirect('/book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$book = Book::where('id', $id)->delete();

        $book = Book::findorfail($id);
        $book->delete();

        $path = "cover/";
        File::delete($path . $book->cover);

        return redirect('/book');
    }
}
