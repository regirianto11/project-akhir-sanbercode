<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded=[];

    public function categorie(){
        return $this->belongsTo('App\Categorie','categorie_id');
    }
}
