@extends('partials.master')

  @section('tittle','Detail Categories')
@section('content')
<div class="card">
      <div class="card-header d-flex justify-content-between">
        <a href="/categorie/create" class="btn btn-primary card-title">Add New Categorie</a>

      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Categorie</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($categories as $key =>   $categorie )
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$categorie->name}}</td>
              <td class="d-flex">
                <a href="/categorie/{{$categorie->id}}/edit" class="btn btn-success mr-2">Edit</a>
                <form action="/categorie/{{$categorie->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger" value="Delete">
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Categorie</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection

@push('script')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
   $(function () {
    $('#example1').DataTable();
  });
</script>
@endpush