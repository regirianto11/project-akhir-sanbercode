@extends('partials.master')

  @section('tittle','List Books')
@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Form Edit Categorie</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="/categorie/{{$categorie->id}}" method="Post">
    @csrf
    @method('put')
    <div class="card-body">
        <div class="form-group">
          <label for="title">Name Categorie</label>
          <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie" value="{{$categorie->name}}">
          @error('categorie')
              <div class="alert alert-danger mt-1">{{ $message }}</div>
          @enderror
        </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>

@endsection
