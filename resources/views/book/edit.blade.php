@extends('partials.master')

  @section('tittle','Edit Books')
@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Form edit books</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="/book/{{$book->id}}" method="Post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="card-body">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Title book" value="{{$book->title}}">
          @error('title')
              <div class="alert alert-danger mt-1">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="umur">Author </label>
          <input type="text" class="form-control" name="author" id="author" placeholder="Author" value="{{$book->author}}">
          @error('author')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="categorie">Categorie </label>
          <select class="custom-select form-control-border" id="categorie" name="categorie">
            @foreach($categories as $categorie)
             <option value="{{$categorie->id}}">{{$categorie->name}}</option>
            @endforeach
          </select>
             @error('categorie')
               <div class="alert alert-danger mt-1 ">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="year">Year </label>
          <input type="number" class="form-control" name="year" id="year" placeholder="year" value="{{$book->year}}">
          @error('year')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="cover">cover </label>
          <input type="file" class="form-control" name="cover" id="cover" placeholder="cover" value="{{$book->cover}}">
          @error('cover')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="plot">Plot </label>
          <textarea class="form-control" rows="3" name="plot" placeholder="Plot Book">{{$book->plot}}</textarea>
          @error('plot')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
@endsection
