@extends('partials.master')

  @section('tittle','List Books')
@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Form Tambah</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="/book" method="Post" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Title book">
          @error('title')
              <div class="alert alert-danger mt-1">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="umur">Author </label>
          <input type="text" class="form-control" name="author" id="author" placeholder="Author">
          @error('author')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
          <label for="categorie">Categorie </label>
          <div class="form-group">
            <select class="custom-select form-control-border" id="exampleSelectBorder" name="categorie">
              @foreach($categories as $categorie)
              <option value="{{$categorie->id}}">{{$categorie->name}}</option>
              @endforeach
            </select>
               @error('categorie')
               <div class="alert alert-danger mt-1 ">{{ $message }}</div>
              @enderror
          </div>
        <div class="form-group">
          <label for="year">Year </label>
          <input type="number" class="form-control" name="year" id="year" placeholder="year">
          @error('year')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="cover">cover </label>
          <input type="file" class="form-control" name="cover" id="cover" placeholder="cover">
          @error('cover')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="plot">Plot </label>
          <textarea class="form-control" rows="3" name="plot" placeholder="Plot Book"></textarea>
          @error('plot')
              <div class="alert alert-danger mt-1 ">{{ $message }}</div>
          @enderror
        </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>

@endsection
