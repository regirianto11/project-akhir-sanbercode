@extends('partials.master')

  @section('tittle','Detail Book')
@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">{{$book->title}} book</h3>
  </div>
  <!-- ini content -->
    <div class="container my-4 mx-4">
      <div class="row">
        <h2 class="book__title">{{$book->title}}</h2>
      </div>
      <div class="row">
        <div class="col">
          <img class="book__cover" src="{{ asset('cover/' . $book->cover) }}" alt="{{$book->title}}"  />
        </div>
        <div class="col">
          <div class="book__info">
            
            <h3>Information</h3>
            <h4>Author</h4>
            <p>{{$book->author}}</p>
            <h4>Release Date</h4>
            <p>{{$book->year}}</p>
            <h4>Categorie</h4>
            <p>{{$book->categorie->name}}</p>
          </div>
        </div>
      </div>
    </div>
    
    <div class="book__overview my-4 mx-4">
      <h3>Overview</h3>
      <p>{{$book->plot}}</p>
    </div>
</div>

@endsection

@push('script')
  <style>
    .book__cover {
      width: 100%;
    }
  </style>
    
@endpush
