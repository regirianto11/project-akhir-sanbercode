@extends('partials.master')

  @section('tittle','List Books')
@section('content')
  <div class="card">
      <div class="card-header d-flex justify-content-between">
        <a href="/book/create" class="btn btn-primary card-title">Add New Book</a>

      </div>
      <!-- /.card-header -->
      <div class="card-group my-4 mx-4">

        @foreach ($books as $key => $book)
        <div class="card mx-2 my-2" >
          <img src="{{ asset('cover/' . $book->cover) }}" class="card-img-top" style="height: 580" alt="{{$book->title}}"> 
          <div class="card-body">
            <h5 class="card-title mb-2">{{$book->title}}</h5>
            <p class="card-text mb-0">Author  : {{$book->author}}</p>
            <p class="card-text ">Category    : {{$book->categorie->name}}</p>
            <p class="card-text"><small class="text-muted">{{$book->year}}</small></p>
          </div>
          <div class="card-body">
            <form action="/book/{{$book->id}}" method="POST">
              @csrf
              @method('delete')
              <a href="/book/{{$book->id}}" class="btn btn-info">Details</a>
              <a href="/book/{{$book->id}}/edit" class="btn btn-success">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger">
             </form>
          </div>
        </div>
        @endforeach
        
      </div>
      <!-- /.card-body -->
    
      

    </div>
@endsection

@push('script')
  <style>
    .card-group {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
    }
    .card-title {
      font-weight: bold;
    }
  </style>
  <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $('#example1').DataTable();
    });
  </script>
@endpush
