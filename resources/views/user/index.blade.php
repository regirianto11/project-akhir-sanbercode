@extends('partials.master')

  @section('tittle','User')
@section('content')
<div class="card">
      <div class="card-header d-flex justify-content-between">
        <h3 class="card-title">List of users</h3>

      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>E-mail</th> 
            <th>Telp</th> 
            <th>Join</th> 
            <th>Action</th> 
            
          </tr>
          </thead>
          <tbody>
            @foreach($users as $key => $user)
              <tr>
                <td>{{$key +1}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->telp}}</td>
                <td>{{date('Y-M-d', strtotime($user->created_at))}}</td>
                <td>
                  <a href="/user/{{$user->id}}" class="btn btn-primary">Details</a>
                </td>

              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>E-mail</th>
              <th>Telp</th>
              <th>Join</th>            
              <th>Action</th> 

              
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection

@push('script')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
   $(function () {
    $('#example1').DataTable();
  });
</script>
@endpush